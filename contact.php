<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="photos/favicon.png">
  <title>Freelance Studios</title>
<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Josefin+Sans' rel='stylesheet' type='text/css'>
  <!--<link href="css/tem.css" rel="stylesheet">-->
  <script src="../../assets/js/ie-emulation-modes-warning.js"></script><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include "header.php" ; ?>

<!-- Start Page Content =============================================================-->

<div class="container-fluid"><!-- Start Page Container-->

  <h3>Please fill out the form below to contact us. We look forward to hearing from you!</h3>

<!-- Start Submission Form Area-->
<div id="sub-form" class="container-fluid">
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">NAME</label>
    <input type="email" class="form-control" id="exampleInputEmail1"
    placeholder="First & Last Name">
  </div>
  <div class="form-group">
    <label for="exampleInputemail1">PHONE</label>
    <input type="email" class="form-control" id="exampleInputPassword1"
    placeholder="(###) ###-####">
  </div>
  <div class="form-group">
    <label for="exampleInputemail1">EMAIL</label>
    <input type="email" class="form-control" id="exampleInputPassword1"
    placeholder="user@example.com">
  </div>
  <div class="row">
  <div class="col-md-4">
    <label>Reason For Contact</label>
    <div class="checkbox">
      <label><input type="checkbox">Book A Studio Session</label>
    </div>  
    <div class="checkbox">
      <label><input type="checkbox">Private Lessons</label>
    </div>  
    <div class="checkbox">
      <label><input type="checkbox">Pricing Information</label>
    </div>
  </div>
   <div class="col-md-8">
    <label>Message</label>
    <textarea class="form-control" rows="3" placeholder="Write us."></textarea>
  </div>
  </div> <!--End Row-->
<button type="submit" class="btn btn-default">SUBMIT</button>
</form>
</div  > <!--End Sub-Form Container-->

</div> <!--End Container Fluid--> 

<!-- Start Footer ==================================================-->

<?php include "footer.php" ; ?>

</body>
</html>
