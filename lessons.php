<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="photos/favicon.png">
  <title>Freelance Studios</title>
<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Josefin+Sans' rel='stylesheet' type='text/css'>
  <!--<link href="css/carousel.css" rel="stylesheet">-->
  <!--<link href="css/tem.css" rel="stylesheet">-->
  <script src="../../assets/js/ie-emulation-modes-warning.js"></script><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include "header.php" ; ?>

<!-- Start Page Content =============================================================-->

<div class="container-fluid">

  <div class="row">

    <div class="col-lg-4">
      <div class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="photos/mitch_guitar-1.jpg" alt="Guitar">
            <div class="carousel-caption"></div>
          </div>

          <div class="item">
            <img src="photos/mitch_piano-1.jpg" alt="Piano">
            <div class="carousel-caption"></div>
          </div>

          <div class="item">
            <img src="photos/studio_instruments-1.jpg" alt="Sax and Vocals">
            <div class="carousel-caption"></div>
          </div>
        </div> <!--End Slide Wrapper-->
      </div> <!--End Carousel-->
    </div> <!--End Col 8-->

    <div class="col-lg-8">

       <h3>Are you looking to finally pick up that instrument youʼve so long dreamed of playing? Are you looking to prepare for auditions, performances, and Nyssma ratings? We are here to help!</h4>

       <h4>Available lessons in: piano, clarinet, saxophone, vocal, guitar, and bass.</h4>
    

    </div><!--End Col 4-->

  </div> <!--End Row-->

</div> <!--End Container-->

<!-- Start Footer ==================================================-->

<?php include "footer.php" ; ?>

</body>
</html>
