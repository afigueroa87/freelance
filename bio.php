<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="photos/favicon.png">
  <title>Freelance Studios</title>
<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Josefin+Sans' rel='stylesheet' type='text/css'>
  <!--<link href="css/tem.css" rel="stylesheet">-->
  <script src="../../assets/js/ie-emulation-modes-warning.js"></script><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include "header.php" ; ?>

<!-- Start Page Content =============================================================-->

<div id="main-bio" class="container-fluid">
        <div class="row featurette">
            <h1 class="featurette-heading">"Never forget about the musicality of the work." <span id="name"> &ndash; Mitch Lance</span></h1>
            
            <img id="bio1" class="img-responsive shadow" SRC="photos/index_mitch_1.jpg" >
            
            <p>Growing up in a family of musicians, made it inevitable that I would start a career in the music industry. I graduated from Berklee College of Music with a Bachelor’s Degree in Contemporary Writing and Music Production. Musically, I grew up in the 50’s, 60’s, and 70’s, so the blues are in my blood, while “original” R&B is in my soul. </p>
            <p>While my friends were listening to contemporary artists and other mainstream pop artists, my cd player was spinning one of my favorite compilation albums, “Atlantic Rhythm And Blues 1947-1974.” Sound waves from Wilson Pickett, Otis Redding, Sam and Dave, Joe Tex, Aretha Franklin, and Eddie Floyd were flowing into my ears, and embedded in my brain. I grew up in recording studios, tagging along with my father who would do session after session, and gig after gig. I was subconsciously growing an understanding for recording, production, and overall musicality.</p>

            <p>During high school, I got heavily involved in music production and being a producer. I would sit in the studio for days at a time writing tracks and songs. I would listen to my favorite songs and try to emulate their sound, thus growing my understanding for contemporary production. Production takes you on journeys, each of which open up new doors and possibilities.</p>

            <p>Today, I work with all styles of music. I always look for unique and different sounds. Every project I work on presents new challenges and requires its very own distinct sound. One of the greatest lessons I had to learn about production was to never forget about the musicality of the work, and I emphasize that in every piece of art I produce.</p>
            
        </div><!--End Row Feat-->
        
    </div><!--End Bio section-->

</div> <!--End Container Fluid--> 

<!-- Start Footer ==================================================-->

<?php include "footer.php" ; ?>

</body>
</html>
