<div class="container-fluid"><!-- Start Main Container-->

  <div class="row row-fl-logo">

<!-- Freelance Logo =====================================================-->

      <a href="index.php"> <img class="img-responsive freelance-logo col-md-4" src="photos/FLSTUDIOS-trans.png"> </a>

<!-- Navbar =============================================================-->

    <div class=" col-md-8 header">
        <ul class="nav nav-pills nav-justified"> 
          <li role="presentation"><a href="studio.php">STUDIO</a></li>
          <li role="presentation"><a href="lessons.php">LESSONS</a></li>
          <li role="presentation"><a href="music.php">MUSIC</a></li>
          <li role="presentation"><a href="bio.php">BIO</a></li>
          <li role="presentation"><a href="contact.php">CONTACT</a></li>
        </ul>
    </div>  <!-- End Header/Navbar-->

  </div>  <!-- End Row-->
</div> <!--End Header Container Fluid--> 
