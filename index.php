<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="photos/favicon.png">
  <title>Freelance Studios</title>
<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Josefin+Sans' rel='stylesheet' type='text/css'>
  <!--<link href="css/tem.css" rel="stylesheet">-->
  <script src="../../assets/js/ie-emulation-modes-warning.js"></script><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include "header.php" ; ?>

<!-- Start HP Page Content =============================================================-->

<br /> 

   <h3>At Freelance Studios, we are looking to help you achieve your musical vision. Whether you are looking for musical production, vocal or instrumental recording, or post- production services, weʼve got you covered. Come create your musical memories with us today!</h3>

<br />

<div class="container">
    <div class="col-md-12">
        <div class="carousel slide" id="myCarousel">
          <div class="carousel-inner">

                <div class="item active">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                    <img src="photos/mitch_guitar-1.jpg" class="img-responsive">
                  </div>
                </div>

                <div class="item">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                    <img src="photos/studio_amps_1.jpg" class="img-responsive">
                  </div>
                </div>

                <div class="item">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                    <img src="photos/mitch_piano-1.jpg" class="img-responsive">
                  </div>
                </div>

                <div class="item">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                   <img src="photos/studio_instruments-1.jpg" class="img-responsive">
                 </div>
                </div>

                <div class="item">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                    <img src="photos/index_studio_1.jpg" class="img-responsive">
                  </div>
                </div>

                <div class="item">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                    <img src="photos/studio_amps_2.jpg" class="img-responsive">
                  </div>
                </div>

          </div>
        </div>
    </div>
</div>



<?php include "footer.php" ; ?>

<script>
  $('#myCarousel').carousel({
  interval: 3000
});

$('.carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  if (next.next().length>0) {
 
      next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');
      
  }
  else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
     
  }
});
</script>

</body>
</html>
