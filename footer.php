<!-- Start Footer ==================================================-->

<footer class="footer">
  <a href="contact.php"><button type="button" class="btn btn-default btn-lg btn-block">Book A Session</button></a>
    <div class="container">
      <p>© 2015 Freelance Studios - Powered by <a href="#">JAAM</a></p>
    </div>
</footer> <!--End Footer-->

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/dropdown.js"></script>
<script src="js/button.js"></script>
<script src="js/collapse.js"></script>
<script src="js/bootstrap.js"></script>