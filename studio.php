<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="photos/favicon.png">
  <title>Freelance Studios</title>
<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Josefin+Sans' rel='stylesheet' type='text/css'>
  <!--<link href="css/tem.css" rel="stylesheet">-->
  <script src="../../assets/js/ie-emulation-modes-warning.js"></script><!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include "header.php" ; ?>

<!-- Start Page Content =============================================================-->

<div class="row">

  <div class="col-lg-6"> 
            
    <img class="img-responsive studio-photo shadow" src="photos/studio.jpg">

  </div> <!--End Img Column--> 

  <div class="col-lg-6">

    <h2>Do you have songs that you wish you could get recorded? Are you looking to finish that EP or Mixtape and jumpstart your career? Are you looking to find your musical identity? </h2>
    <h2>At Freelance Studios, you will receive a personal experience to best uncover that missing link to your music. Our producers, backed with years of experience and knowledge, are here to guide you along the journey to finding that special sound and give your songs the shine they deserve.</h2>
    <div class="pull-left">
      <h2>Services Offered:</h2>
        <ul>
            <li class="bullet">Music Production</li>
            <li class="bullet">Recording/Editing</li>
            <li class="bullet">Mixing</li>
            <li class="bullet">Jingle Writing</li>
            <li class="bullet">Film/TV Scoring</li>
            <li class="bullet">Arranging/Orchestration</li>
        </ul>
    </div>
    <div class="pull-right">
     <h2>Services Offered:</h2>
      <ul>
          <li class="bullet">Music Production</li>
          <li class="bullet">Recording/Editing</li>
          <li class="bullet">Mixing</li>
          <li class="bullet">Jingle Writing</li>
          <li class="bullet">Film/TV Scoring</li>
          <li class="bullet">Arranging/Orchestration</li>
      </ul>
    </div>
   

  </div> <!--End Text Column--> 



            


</div> <!--End Row-->

<?php include "footer.php" ; ?>


</body>
</html>
